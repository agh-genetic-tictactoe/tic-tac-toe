module Player where

import Game
import System.IO

-- |Puts an image of the board to the screen
showBoard :: Board -> IO ()
showBoard brd = putStr $ "\n" ++ lin 0 ++ linSp ++ lin 1 ++ linSp ++ lin 2 ++ "\n"
    where brdS = fmap (\x -> case x of
            Nth -> "   "
            O -> " O "
            X -> " X ") brd
          lin n = (brdS !! (n*3)) ++ "|" ++ (brdS !! (n*3+1)) ++ "|" ++ (brdS !! (n*3+2)) ++ "\n"
          linSp = "-----------\n"

-- |Changed the board along with the player's move
moveUser :: (Int, Int) -> Board -> Maybe Board
moveUser (x, y) brd = if x < 1 || y < 1 || x > 3 || y > 3 || (brd !! indx) /= Nth then Nothing 
    else Just (take indx brd ++ [X] ++ drop (indx + 1) brd)
    where indx = 3*(y-1) + x-1

parseWeigths :: String -> Gene
parseWeigths inp = fmap read $ words inp

parseInput :: String -> (Int, Int)
parseInput input = if length inp /= 2 then error " " else (\(x:y:xx) -> (x, y)) inp
    where inp = fmap read $ words input

-- |Player's turn
userTurn :: Gene -> Board -> IO Board
userTurn gn brd = do
    showBoard brd
    input <- fmap parseInput getLine
    let boardM = moveUser input brd
    let board = case boardM of
                    Nothing -> brd
                    Just b -> b
    if boardM == Nothing then userTurn gn board else case winning board of
        Nth -> if tie board then return board else comTurn gn board
        _ -> return board

-- |Gene's turn
comTurn :: Gene -> Board -> IO Board
comTurn gn brd = do
    showBoard brd
    let board = moveGene gn O brd
    case winning board of
        Nth -> if tie board then return board else userTurn gn board
        _ -> return board

-- |The function to call to start the game
playUser :: Int -> FilePath -> IO ()
playUser turn file = do
    com <- fmap parseWeigths $ readFile file
    case turn of
        0 -> userTurn com clearBoard >>= showBoard
        1 -> comTurn com clearBoard >>= showBoard
        _ -> putStrLn "Nieprawidłowa funkcja (0 - rozpoczynający człowiek, 1 - rozpoczynający komputer)."
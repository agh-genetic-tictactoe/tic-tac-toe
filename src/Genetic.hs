module Genetic where

import Game (Gene, geneLength, playGenes)
import Control.Applicative
import Control.Arrow (second)
import Control.Monad (liftM, replicateM)
import Control.Monad.Random
import Data.Function (on)
import Data.List (minimumBy, sortBy, nub, maximumBy)
import Data.Ord (comparing)

randomBoolList :: RandomGen g => Rand g [Bool]
randomBoolList = replicateM geneLength getRandom

randomGene :: RandomGen g => Rand g Gene 
randomGene = replicateM geneLength $ getRandomR (-0.25, 1.0)

-- | Chooses genes according to randomly generated [Bool] table.
chooseGenes :: Gene -> Gene -> [Bool] -> Gene
chooseGenes [] g2 _ = g2
chooseGenes g1 [] _ = g1
chooseGenes (h1:t1) (h2:t2) (ch:t3) = if ch
                                      then h1 : (chooseGenes t1 t2 t3)
                                      else h2 : (chooseGenes t1 t2 t3)

-- | Randomly mix two given genes into a new one.
mate :: RandomGen g => Rand g Gene -> Rand g Gene -> Rand g Gene
mate g1 g2 = liftA3 chooseGenes g1 g2 randomBoolList

-- | Change randomly chosen values of gene by random delta.
mutate :: RandomGen g => Gene -> Rand g Gene
mutate gene = liftA (zipWith (+) gene) mutant
  where mutant = replicateM geneLength $ (*) <$> getRandomR (0, 2) <*> getRandomR (-0.1, 0.1)
        
-- | Data structure that defines main parameters of population.
data PopInfo = PopInfo { -- | How many genes are there in the population
                         size      :: Int
                         -- | Possibility of generating a new gene as a crossover of existing ones.
                       , crossover :: Float
                         -- | Percentage of best genes that survives each phase of population.
                       , elitism   :: Float
                         -- | Chance of random mutation occurence for new born genes.
                       , mutation :: Float}

type Population = (PopInfo, [Gene]) 

-- Default parameters of the population 
defaultPop :: PopInfo
defaultPop = PopInfo 100 0.2 0.1 0.5

-- Generate the first population
randomPop :: RandomGen g => PopInfo -> Rand g Population 
randomPop info = liftA2 (,) (pure info) $ replicateM (size info) randomGene

twoM :: Monad m => m a -> m (a, a)
twoM = liftM (\[x,y] -> (x,y)) . replicateM 2

selectParents :: Population -> (Gene, Gene)
selectParents (info, genes) = 
    (maximumBy playGenes $ take half genes 
    ,maximumBy playGenes $ drop half genes) 
    where half = (length genes) `div` 2

evolve :: RandomGen g => Population -> Rand g Population
evolve p@(info, genes) =
      liftA2 (,) (pure info) (liftA ((++) (take idx $ reverse $ sortBy playGenes genes)) (replicateM (size info - idx) (twoM getRandom >>= go)))
      where idx = round (fromIntegral (size info) * (elitism info))
            go (r1,r2) | r1 <= (crossover info) = (mate (pure (fst (selectParents p))) (pure (snd (selectParents p)))) >>= addChild r2 
                       | otherwise = addMutation r2
            addChild r c
                | r <= (mutation info) = mutate c
                | otherwise = return c
            addMutation r
                | r <= (mutation info) = mutate . (genes !!) =<< getRandomR (idx, size info - 1)
                | otherwise = (genes !!) <$> getRandomR (idx, size info - 1)

                
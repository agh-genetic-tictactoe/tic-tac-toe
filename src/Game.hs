module Game where

import Data.List
import Data.Ord
import Data.Hashable
import Data.Bits
import Control.Applicative

-- |Data type representing gene's weights for each feature
type Gene = [Float]
-- |Symbols, which the boards consists of: O, X and the 'Noth', which stands for a free square
data Symbol = Nth | O | X deriving (Show, Enum, Eq)
{- |
  List of nine symbols represeting the board in the following order:

    0 | 1 | 2
   -----------
    3 | 4 | 5
   -----------
    6 | 7 | 8

  where each number stands for index of the coressponding square in the list
-}
type Board = [Symbol]


-- |Nuber of features optimalized
geneLength :: Int
geneLength = 5

-- |Returns 1 when comparing the same arguments, 2 for different and 0 when a symbol is compard to Nth
compareSymbols :: Symbol -> Symbol -> Int
compareSymbols s oth
    | oth == Nth = 0
    | s == oth = 1
    | otherwise = 2


clearBoard :: Board
clearBoard = replicate 9 Nth

every :: Int -> [a] -> [a]
every _ [] = []
every n (x:xs) = (:) x $ every n $ drop (n-1) xs

-- |Return a list of the contents of all the possible straight 3-symbol lines in a board
boardLines :: Board -> [[Symbol]]
boardLines brd = [ take 3 brd
                ,take 3 $ drop 3 brd
                ,drop 6 brd
                ,every 3 brd
                ,every 3 (tail brd)
                ,every 3 (drop 2 brd)
                ,every 4 brd
                ,every 2 $ drop 2 $ init brd]

-- |Returns the victorious symbol if either has won, Nth otherwise
winning :: Board -> Symbol
winning brd = case find (\(x:y:z:xs) -> x /= Nth && x == y && y == z) $ boardLines brd of
    Nothing -> Nth
    Just (x:xs) -> x

-- |Assuming neither symbol has won, tells whether any move is possible
tie :: Board -> Bool
tie = notElem Nth

-- |Based on the active player's symbol and a single line returns the number of a feature the line represents or 0 in case it represents none.
lineState :: Symbol -> [Symbol] -> Int
lineState smb = lineState' 0
    where lineState' st [] = st
          lineState' st (x:xs) = case compareSymbols smb x of
            0 -> lineState' st xs
            1 -> if st < 4 then lineState' (st+1) xs else 0
            2 | st == 0 -> lineState' 4 xs
              | st > 3 -> lineState' (st + 1) xs
              | otherwise -> 0

-- |Takes the active player's weights, its symbol and a board and returns the board's value for the player.
evalBoard :: Gene -> Symbol -> Board -> Float
evalBoard gn smb brd = sum $ zipWith (*) gn $ fmap (fromInteger.toInteger) features
    where features = fmap (\x -> length $ filter (==x) boardState) [1..geneLength]
          boardState = lineState smb <$> boardLines brd

-- |Takes the active player's weights, its symbol and a board and returns the most advantageous move, given the situation.
moveGene :: Gene -> Symbol -> Board -> Board
moveGene gn smb brd = snd $ maximumBy (comparing fst) $ fmap (\b -> (evalBoard gn smb b, b)) $ shuffleBoard brd gn $ moves brd
    where moves = moves' [] []
          moves' movs _ [] = movs
          moves' movs bhead (s:ss) = if s == Nth then moves' ((++) movs [bhead ++ [smb] ++ ss]) (bhead ++ [s]) ss
            else moves' movs (bhead ++ [s]) ss
          boardHash = hash (fmap ((+1) . fromEnum) brd) + hash gn
          shuffleBoard brd gn mvs = shuffle' [head mvs] (tail mvs)
            where shuffle' sh [] = sh
                  shuffle' sh (s:ss) = shuffle' (take piv sh ++ [s] ++ drop piv sh) ss
                    where piv = (boardHash + hash (show sh)) `mod` (length sh + 1)
                  boardHash = hash (fmap ((+1) . fromEnum) brd) `xor` hash gn
          
-- |Takes two genes and stages a single game, starting with the gene passed as the first argument.
playGenes :: Gene -> Gene -> Ordering
playGenes g1 g2 = order $ move True clearBoard
    where move turn brd
            | win /= Nth = win
            | tie brd = Nth
            | otherwise =
              move (not turn) $
                moveGene (if turn then g1 else g2) (if turn then O else X) brd
            where win = winning brd
          order smb = case smb of
            Nth -> EQ
            O -> GT
            X -> LT
{-# LANGUAGE NamedFieldPuns, TupleSections #-}

module Main where

import Genetic
import Game
import Player
import System.IO
import Control.Monad
import Control.Monad.Random

iterateUntil :: Monad m => (a -> Bool) -> (a -> m a) -> a -> m a
iterateUntil stop f = go
    where go x | stop x = return x
               | otherwise = f x >>= go

maxGenerations :: Int
maxGenerations = 5000

step :: Int -> Population -> IO Population
step acc pop = do
  evaled <- evalRandIO (evolve pop)
  putStrLn $ (show acc) ++ " " ++ (show . head . snd $ evaled)
  case maxGenerations == acc of
    True -> return evaled
    False ->  (step (acc+1)) evaled

parseOutput :: Gene -> String
parseOutput [f] = show f
parseOutput (f:fs) = (show f) ++ " " ++ (parseOutput fs)
    
main = do 
  first <- evalRandIO (randomPop defaultPop)
  result <- step 0 first
  putStrLn (show "Done")
  writeFile "weights.gene" $ parseOutput . head . snd $ result
module GeneticTest where
import Test.HUnit
import Test.QuickCheck
import Test.QuickCheck.Monadic
import Genetic

testMate = assertBool "Mate test failed." (twoM randomGene >>= triple >>= check)
   where triple (g1, g2) = (g1, g2, mate g1 g2)
         check | ( _, _, _) = True
               | ([], _, _) = False
               | (_, [], _) = False
               | (_, _, []) = False
               | (h1:t1, h2:t2, h3:t3) = check (t1, t2, t3) && ((h1 == h3) || (h2 == h3))

testMutate = assertBool "Mutation test failed." (randomGene >>= pairWithMutant >>= check)
   where pairWithMutant gene = (gene, mutate gene)
         check | ( _, _) = True
               | ([], _) = False
               | (_, []) = False
               | (h1:t1, h2:t2) = check (t1, t2) && abs (h1 - h2) <= 0.1

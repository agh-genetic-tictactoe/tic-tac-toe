import Test.HUnit
import Test.QuickCheck
import GameTest

main :: IO ()
main = do
    runTestTT gameUnitTests
    mapM_ quickCheck gameQCTests
    putStr "Done"
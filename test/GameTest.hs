module GameTest where
import Test.HUnit
import Test.QuickCheck
import Control.Monad
import Control.Monad.Random
import Game

gameUnitTests = TestList [TestLabel "Symbols compare" testCompareSymbols
                    , TestLabel "Check for game end" testEndGame
                    , TestLabel "Check board evaluation" testEvaluation
                    , TestLabel "Check making a move based on the weights" testMakingMove]

testCompareSymbols = TestList (fmap TestCase 
   [assertEqual "X-O" 2 (compareSymbols X O)
   ,assertEqual "X-X" 1 (compareSymbols X X)
   ,assertEqual "X-Nth" 0 (compareSymbols X Nth)
   ,assertEqual "O-Nth" 0 (compareSymbols O Nth)
   ,assertEqual "O-O" 1 (compareSymbols O O)]
    )

testEndGame = TestList (fmap TestCase
  [assertBool "Tie case #1" (tie [O, O, X, X, X, O, O, X, X])
  ,assertBool "Tie case #2" (tie [X, O, O, X, O, X, O, X, O])
  ,assertEqual "O wins horizontal bottom" O (winning [X, Nth, Nth, Nth, X, Nth, O, O, O])
  ,assertEqual "O wins diagonal RightTop-LeftBottom" O (winning [X, X, O, X, O, X, O, X, O])
  ,assertEqual "X wins vertical centre" X (winning [Nth, X, O, O, X, Nth, O, X, Nth])
  ,assertEqual "X wins horizontal top" X (winning [X, X, X, O, Nth, O, O, X, Nth])
  ,assertEqual "No one wins" Nth (winning [O, O, Nth, Nth, X, Nth, Nth, Nth, X])
  ])

testEvaluation = TestList (fmap TestCase
  [assertEqual "Case #1" 4.5 (evalBoard (replicate 5 0.75) O [O, O, Nth, Nth, X, Nth, Nth, Nth, X])
  ,assertEqual "Case #2" (-0.3) (evalBoard [0.1, 0.23, 0.8, -0.1, -0.6] X [O, O, Nth, Nth, X, Nth, Nth, Nth, X])
  ,assertEqual "Case #3" (0.9) (evalBoard [0.1, 0.23, 0.8, -0.1, -0.6] X [O, O, X, Nth, X, Nth, X, Nth, O])
  ])

testMakingMove = TestList (fmap TestCase
  [assertEqual "Case #1" [O,O,X,Nth,X,Nth,X,Nth,O] (moveGene [0.1, 0.23, 0.8, -0.1, -0.6] X [O, O, X, Nth, X, Nth, Nth, Nth, O])
  ,assertEqual "Case #2" [Nth,O,X,Nth,X,Nth,O,Nth,O] (moveGene [0.3, 0.2, 0.9, -0.1, -1.6] O [Nth, O, X, Nth, X, Nth, Nth, Nth, O])
  ,assertEqual "Case #3" [Nth,Nth,Nth,Nth,O,Nth,Nth,Nth,Nth] (moveGene [0.3, 0.2, 0.9, -0.1, -1.6] O clearBoard)
  ])

gameQCTests = [prop_RevLineState, prop_Win]

instance Arbitrary Symbol where
  arbitrary = liftM toEnum (choose (0, 2)) :: Gen Symbol

prop_RevLineState = forAll (replicateM 3 arbitrary :: Gen [Symbol]) $ \line -> lineState O line == lineState O (reverse line)

arbitraryWin :: Gen Board
arbitraryWin = arbitraryWin' (replicate 9 Nth)
  where arbitraryWin' brd = if winning brd /= Nth then pure brd else (replicateM 9 arbitrary) >>= arbitraryWin'

prop_Win = forAll arbitraryWin $ \board -> or $ fmap ((\st -> st == 3 || st == 6) . lineState O) $ boardLines board